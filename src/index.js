// App
import { createModel } from './app/createModel.js';
import { createViewModel } from './app/createViewModel.js';
import { createView } from './app/createView.js';

// Components
import './components/currentYear.js';
import './components/movieCard.js';

// Helpers
import { setStateLocalStorage } from './helpers/setStateLocalStorage.js';

const model = createModel();
const view = createView();
const viewModel = createViewModel(model);

// ViewModel -> View
viewModel.bindCount(view.renderCount);
viewModel.bindError(view.renderError);
viewModel.bindResults(view.renderList);
viewModel.bindSearches(view.renderSearchList);
viewModel.bindLoad(view.renderLoading);

// View -> ViewModel
view.onSearchInput(viewModel.handleSearchSubmit);
view.onSearchBlur();
view.onTagClick(viewModel.handleTagClick);
view.onTagRemove(viewModel.handleTagRemove);
view.onSearchScroll();

// Init app
viewModel.init();

model.subscribe(console.log);
model.subscribe(setStateLocalStorage);
