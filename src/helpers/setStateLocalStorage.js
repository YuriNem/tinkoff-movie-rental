export const setStateLocalStorage = ({ count, results, searches }) => 
    (localStorage.setItem('state', JSON.stringify({ count, results, searches })));
