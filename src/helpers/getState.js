export const getState = () => ({ 
    ...(JSON.parse(localStorage.getItem('state')) || 
    { 
      count: 0, 
      results: [], 
      searches: [],
    }),
    error: false,
    load: false,
  });
