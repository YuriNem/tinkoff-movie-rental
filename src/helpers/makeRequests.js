export const makeRequests = (urls, maxRequests) => new Promise((resolve, reject) => {
  const result = [];
  const visitedUrls = [];

  let count = maxRequests;

  const makeRequest = (url, index) => {
    const makeNextRequest = data => {
      result[index] = data;
      visitedUrls[index] = url;

      const nextIndex = index + maxRequests;

      if (nextIndex >= urls.length) {
        count -= 1;
        if (count === 0) {
          resolve(result);
        }
      } else {
        makeRequest(urls[nextIndex], nextIndex);
      }
    };

    if (visitedUrls.includes(url)) {
      makeNextRequest(result[visitedUrls.indexOf(url)]);
    } else {
      fetch(url)
        .then(res => res.json())
        .then(data => makeNextRequest(data))
        .catch(error => reject(error));
    }
  };

  for (let i = 0; i < maxRequests; i++) {
    makeRequest(urls[i], i);
  }
});
