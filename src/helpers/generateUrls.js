export const generateUrls = (url, searchTerm, totalPages) => {
    const urls = [];
    for (let i = 2; i <= totalPages; i++) {
        urls.push(`${url}&s=${searchTerm}&page=${i}`);
    }
    return urls;
};
