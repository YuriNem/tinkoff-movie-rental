import { createStore } from '../helpers/createStore.js';
import { mapMovie } from '../helpers/mapMovie.js';
import { getState } from '../helpers/getState.js';
import { makeRequests } from '../helpers/makeRequests.js';
import { generateUrls } from '../helpers/generateUrls.js';

const cache = {};

export const createModel = () =>
  createStore(
    getState(),
    store => ({
      search: async (currentState, searchTerm) => {
        store.setState({
          count: 0,
          results: [],
          error: false,
          searches: [searchTerm].concat(
            currentState.searches.filter(term => term !== searchTerm),
          ),
          load: true,
        });

        if (cache[searchTerm.toLowerCase()]) {
          return { ...cache[searchTerm.toLowerCase()], load: false };
        }

        try {
          const dataFetch = await fetch(
            `http://www.omdbapi.com/?type=movie&apikey=1b6de150&s=${searchTerm}`
          ).then(r => r.json());

          if (dataFetch.Response === 'True') {
            if (dataFetch.totalResults > 10) {
              const totalPages = Math.ceil(dataFetch.totalResults / 10);
              const urls = generateUrls(
                'http://www.omdbapi.com/?type=movie&apikey=1b6de150', 
                searchTerm, 
                totalPages,
              );

              const data = await makeRequests(urls, totalPages - 1);

              const result = {
                count: dataFetch.totalResults,
                results: data.reduce((acc, { Search }) => 
                  ([...acc, ...Search]), dataFetch.Search).map(mapMovie),
              };

              cache[searchTerm.toLowerCase()] = result;
              return { ...result, load: false };
            }

            const result = {
              count: dataFetch.totalResults,
              results: dataFetch.Search.map(mapMovie),
            };

            cache[searchTerm.toLowerCase()] = result;
            return { ...result, load: false };

          } else {
            const result = {
              error: dataFetch.Error,
              load: false,
            };

            cache[searchTerm.toLowerCase()] = result;
            return { ...result, load: false };
          }

        } catch (error) {
          return {
            error,
            load: false,
          };
        }
      },
      removeTag: (currentState, searchTerm) => {
        return {
          searches: currentState.searches.filter(term => term !== searchTerm),
        };
      },
    }),
  );
