import { clearNode } from '../helpers/clearContainer.js';
import { getDeclension } from '../helpers/getDeclension.js';
import { debounce } from '../helpers/debounce.js';

const dMovies = getDeclension('фильм', 'фильма', 'фильмов');

export const createView = () => {
  // Search list
  const resultsContainer = document.querySelector('.search__movies');
  const resultsHeading = document.querySelector('.search__heading');

  // Tags list
  const searchTags = document.querySelector('.search__tags');

  // Form
  const searchForm = document.querySelector('.search__form');
  const searchInput = document.querySelector('.search__input');

  // Renderers
  const renderList = (results) => {
    const list = document.createDocumentFragment();

    results.forEach((movieData) => {
      const movie = document.createElement('movie-card');

      movie.poster = movieData.poster;
      movie.title = movieData.title;
      movie.year = movieData.year;
      movie.link = movieData.link;

      list.appendChild(movie);
    });

    clearNode(resultsContainer);
    resultsContainer.classList.remove('search__movies_loading');
    resultsContainer.appendChild(list);
  };

  const renderSearchList = (terms) => {
    const list = document.createDocumentFragment();

    terms.forEach((movie) => {
      const tag = document.createElement('a');
      tag.classList.add('search__tag');
      tag.href = `/?search=${movie}`;
      tag.textContent = movie;
      tag.dataset.movie = movie;

      list.appendChild(tag);
    });

    clearNode(searchTags);
    searchTags.appendChild(list);
  };

  const renderCount = (count) => {
    resultsHeading.textContent = `Нашли ${count} ${dMovies(count)}`;
  };

  const renderError = (error) => {
    clearNode(resultsContainer);
    resultsContainer.classList.remove('search__movies_loading');
    resultsHeading.textContent = error;
  };

  const renderLoading = (load) => {
    if (load) {
      const loadingDiv = document.createElement('div');
      loadingDiv.classList.add('search__loading');
      clearNode(resultsHeading);
      clearNode(resultsContainer);
      resultsContainer.classList.add('search__movies_loading');
      resultsContainer.appendChild(loadingDiv);
    }
  };

  // Events
  const onSearchInput = (_listener) => {
    const listener = (event) => {
      event.preventDefault();
      
      if (searchInput.value) {
        _listener(searchInput.value);
      }
    };

    searchInput.addEventListener('input', debounce(listener, 1000));
    return () => searchInput.removeEventListener('input', listener);
  };

  let tagClicked = false;

  const onSearchBlur = () => {
    let blur = false;
    const listener = (event) => {
      event.preventDefault();

      if (blur) {
        blur = false;
        return;
      }

      searchInput.focus();
      setTimeout(() => {
        if (tagClicked) {
          tagClicked = false;
          return;
        }

        blur = true;
        searchInput.blur();
      }, 200);
    };

    searchInput.addEventListener('blur', listener);
    return () => searchInput.removeEventListener('blur', listener);
  };

  const onTagClick = (_listener) => {
    const listener = (event) => {
      event.preventDefault();

      if (event.target.classList.contains('search__tag') && !event.altKey) {
        tagClicked = true;

        _listener(event.target.dataset.movie);
        searchInput.value = event.target.dataset.movie;
      }
    };

    searchTags.addEventListener('click', listener);
    return () => searchTags.removeEventListener('click', listener);
  };

  const onTagRemove = (_listener) => {
    const listener = (event) => {
      event.preventDefault();

      if (event.target.classList.contains('search__tag') && event.altKey) {
        tagClicked = true;

        _listener(event.target.dataset.movie);
      }
    };

    searchTags.addEventListener('click', listener);
    return () => searchTags.removeEventListener('click', listener);
  };

  const onSearchScroll = () => {
    const listener = (event) => {
      event.preventDefault();

      if (window.pageYOffset > searchForm.offsetTop + 75) {
        searchForm.classList.add('search__form_scroll');
      } else {
        searchForm.classList.remove('search__form_scroll');
      }
    };

    window.addEventListener('scroll', listener);
    return () => window.removeEventListener('scroll', listener);
  };

  return {
    renderList,
    renderCount,
    renderError,
    renderSearchList,
    renderLoading,
    onSearchInput,
    onSearchBlur,
    onTagClick,
    onTagRemove,
    onSearchScroll,
  };
};
